package com.thebest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoneAppetitApplication {

	public static void main(String[] args) {
		SpringApplication.run(BoneAppetitApplication.class, args);
	}

}
