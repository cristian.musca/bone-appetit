package com.thebest.repository;

import com.thebest.model.adminModel.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface RestaurantRepository extends JpaRepository<Restaurant, Long> {
    @Query("Select r from Restaurant r where manager.idManager=:idManager ")
    List<Restaurant> findAllByManagerId(@Param("idManager")Long idManager);
    Optional<Restaurant> findById(Long idRestaurant);
}
