package com.thebest.repository;

import com.thebest.model.clientModel.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Query("SELECT coalesce(max(ch.idOrderCustomer), 0) FROM OrderCustomer ch")
    Long getMaxId();
    Customer findByEmailAndPassword(String email, String password);
    long countByEmailAndPassword(String email, String password);
}
