package com.thebest.repository;

import com.thebest.model.clientModel.OrderCustomer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface OrderCustomerRepository extends JpaRepository<OrderCustomer, Long> {
    @Query("SELECT coalesce(max(ch.idOrderCustomer), 0) FROM OrderCustomer ch")
    Long getMaxId();

}
