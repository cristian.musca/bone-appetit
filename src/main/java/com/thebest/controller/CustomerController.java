package com.thebest.controller;

import com.thebest.model.clientModel.Customer;
import com.thebest.model.clientModel.Status;
import com.thebest.repository.CustomerRepository;
import com.thebest.repository.RestaurantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Optional;

@Controller
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Autowired
    private HttpSession httpSession;

    @GetMapping("/displaySignUpCustomer")
    public String displaySignUpCustomer(Model model){
        model.addAttribute("customer", new Customer());

        return "signUp-customer";
    }

    @PostMapping("/customer/add")
    public String addCustomer(@Valid @ModelAttribute("customer") Customer customer,
                              BindingResult result,
                              RedirectAttributes ra, Model model) {
        if (result.hasErrors()) {
            return "signUp-customer";
        }
        customerRepository.save(customer);

        model.addAttribute("customer", customer);
        model.addAttribute("idCustomer", customer.getIdCustomer());
        System.out.println("Customer id: "+ customer.getIdCustomer());
        model.addAttribute("userName", customer.getCustomerName());
        model.addAttribute("restaurants", restaurantRepository.findAll());
        model.addAttribute("localDate", LocalDate.now());
        model.addAttribute("timestamp", Instant.now());
        model.addAttribute("status", Status.AFFECTED );
        ra.addFlashAttribute("message", "The customer has been saved successfully.");

        return "customer-home";
    }

    @GetMapping("/displayLoginCustomer")
    public String displayLogInCustomer(Model model){

        return "customer-logIn";
    }

    @GetMapping("/autentificare")
    public String autentificare(@RequestParam(value="email",required=false) String email, @RequestParam(value = "password", required = false) String password,Model model){
        Customer customer = customerRepository.findByEmailAndPassword(email, password);
        model.addAttribute("restaurants", restaurantRepository.findAll());
        if(customer != null){
            httpSession.setAttribute("customer", customer);
            model.addAttribute("customer", customer);
            return "customer-home";
        }
        model.addAttribute("customer", new Customer());
        return "/signUp-customer";
    }

    @PostMapping("/customer/reset/{email}")
    public String resetCustomer(@PathVariable("email") String email,
                                @Valid @ModelAttribute("customer") Customer customer,
                                BindingResult result, Model model){
        if (result.hasErrors()) {
            return "customer-reset";
        }
        customerRepository.save(customer);
        model.addAttribute("customer", customer);
        model.addAttribute("idCustomer", customer.getIdCustomer());
        model.addAttribute("restaurants", restaurantRepository.findAll());
        model.addAttribute("localDate", LocalDate.now());
        model.addAttribute("timestamp", Instant.now());
        model.addAttribute("status", Status.AFFECTED );

        return "customer-home";
    }

    @GetMapping("/displayEditCustomer/{idCustomer}")
    public String displayCustomerUpdate(@PathVariable("idCustomer") Long idCustomer, Model model){
        final Optional<Customer>optionalCustomer = customerRepository.findById(idCustomer);
        if(optionalCustomer.isPresent()){
            final Customer customer = optionalCustomer.get();
            model.addAttribute(customer);
            model.addAttribute("customers", customerRepository.findAll());
        }else {
            new IllegalArgumentException("Invalid customer Id:" + idCustomer);
        }


        return "update-customer";
    }

    @PostMapping("/customers/update/{idCustomer}")
    public String updateCustomer(@PathVariable("idCustomer") Long idCustomer, @Valid @ModelAttribute("customer") Customer customer,
                                 BindingResult result){

        if(result.hasErrors()){
            return "update-customer";
        }
        customerRepository.save(customer);

        return "redirect:/customer-list";
    }

    @GetMapping("/customers/delete/{idCustomer}")
    public String deleteCustomer(@PathVariable("idCustomer") Long idCustomer) {
        customerRepository.deleteById(idCustomer);
        return "redirect:/customer-list";
    }

    @GetMapping("/customer-list")
    public String displayCustomersList(Model model) {
        model.addAttribute("customers", customerRepository.findAll());
        return "customer-list";
    }

}