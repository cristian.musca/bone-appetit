package com.thebest.controller;

import com.thebest.model.adminModel.Manager;
import com.thebest.repository.ManagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class ManagerController {

    @Autowired
    ManagerRepository managerRepository;


    @GetMapping("/displayAddAdmin")
    public String displayAddAdmin( Model model) {
        model.addAttribute("manager", new Manager());

        return "add-admin";
    }

    @PostMapping("/admin/add")
    public String addAdmin(@Valid @ModelAttribute("manager") Manager manager, BindingResult result) {
        if (result.hasErrors()) {
            return "add-admin";
        }

        managerRepository.save(manager);
        for(Manager managers: managerRepository.findAll()){
            managers.displayManager();
        }
        return "redirect:/managers-list";

    }


    @GetMapping("/managers-list")
    public String displayManagerList(Model model) {
        model.addAttribute("managers",managerRepository.findAll());
        return "managers-list";
    }

    @GetMapping("/displayEditManager/{idManager}")
    public String displayUpdateForm(@PathVariable("idManager") Long idManager, Model model){
        final Optional<Manager> optionalManager = managerRepository.findById(idManager);
        if (optionalManager.isPresent()) {
            final Manager manager = optionalManager.get();
            model.addAttribute("manager", manager);
        } else {
            new IllegalArgumentException("Invalid restaurant Id:" + idManager);
        }

        return "update-manager";
    }

    @PostMapping("/managers/update/{idManager}")
    public String updateProduct(@PathVariable("idManager") Long idManager,
                                @Valid @ModelAttribute("manager") Manager manager, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "update-manager";
        }
        managerRepository.save(manager);
        return "redirect:/managers-list";
    }

    @GetMapping("/managers/delete/{idManager}")
    public String deleteRestaurant(@PathVariable("idManager") Long idManager) {
        managerRepository.deleteById(idManager);
        return "redirect:/managers-list";
    }

}
