package com.thebest.controller;

import com.thebest.model.adminModel.Product;
import com.thebest.model.adminModel.Restaurant;
import com.thebest.repository.ProductRepository;
import com.thebest.repository.RestaurantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Optional;

@Controller
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    RestaurantRepository restaurantRepository;

    @GetMapping("/displayAddFormProduct/{idRestaurant}")
    public String displayAddFormProduct(@PathVariable(value = "idRestaurant") Long idRestaurant, Model model) {
        Product product = new Product();
        final Optional<Restaurant> optional = restaurantRepository.findById(idRestaurant);
        if (optional.isPresent()) {
            final Restaurant restaurant = optional.get();
            System.out.println("Restaurant Id: " + restaurant.getIdRestaurant());
            model.addAttribute("restaurant", restaurant);
            product.setRestaurant(restaurant);
            model.addAttribute("product", product);
            System.out.println(product.getRestaurant().getRestaurantName());
            System.out.println(product.getRestaurant().getIdRestaurant());

        } else {
            new IllegalArgumentException("Invalid restaurant Id" + idRestaurant);
        }

        return "add-product";
    }

    @PostMapping("/products/add")
    public String addProduct(@Valid @ModelAttribute("product") Product product, Model model,
                             BindingResult result,
                             @RequestParam("imageFile") MultipartFile file){
        if (result.hasErrors()) {
            return "add-product";
        }
        try{
            byte[] byteObjects = convertToBytes(file);
            product.setImage(byteObjects);
            productRepository.save(product);
            product.displayProduct();

        }catch(Exception e){
            e.printStackTrace();
        } finally {
            final Optional<Restaurant> optional = restaurantRepository.findById(product.getRestaurant().getIdRestaurant());
            Product newProduct = new Product();
            if (optional.isPresent()){
                final Restaurant restaurant = optional.get();
                System.out.println("Id restaurant: "+ restaurant.getIdRestaurant());

                newProduct.setRestaurant(restaurant);
                model.addAttribute("restaurant", restaurant);
                model.addAttribute("product", newProduct);
            }
        }
        return "add-product";
    }


    private byte[] convertToBytes(MultipartFile file) throws IOException {
        byte[] byteObjects = new byte[file.getBytes().length];
        int i = 0;
        for (byte b : file.getBytes()) {
            byteObjects[i++] = b;
        }
        return byteObjects;
    }

    @GetMapping("/products-list")
    public String displayProductsList(Model model) {
        model.addAttribute("products", productRepository.findAll());
        return "products-list";
    }

    @GetMapping("/displayEditProduct/{idProduct}")
    public String displayUpdateForm(@PathVariable("idProduct") Long idProduct, Model model){
        final Optional<Product> optionalProduct = productRepository.findById(idProduct);
        if (optionalProduct.isPresent()) {
            final Product product = optionalProduct.get();
            model.addAttribute("product", product);
        } else {
            new IllegalArgumentException("Invalid restaurant Id:" + idProduct);
        }

        return "update-product";
    }

    @PostMapping("/products/update/{idProduct}")
    public String updateProduct(@PathVariable("idProduct") Long idProduct,
                                @Valid @ModelAttribute("product")  Product product, BindingResult result,
                                Model model, @RequestParam("imageFile") MultipartFile file) {
        if (result.hasErrors()) {
            return "update-product";
        }
        try {
            byte[] byteObjects = convertToBytes(file);
            product.setImage(byteObjects);
            productRepository.save(product);
            product.displayProduct();
        }catch(Exception e){
            e.printStackTrace();
        }

        return "redirect:/products-list";
    }

    /*@PostMapping("/products/update/{productName}")
    public String updateRestaurant(@PathVariable("productName") Long idProduct, @Valid @ModelAttribute("restaurant")  RestaurantDTO restaurantDTO, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "update-product";
        }
        restaurantService.save(restaurantDTO);
        return "redirect:/products-list";
    }*/

    @GetMapping("/products/delete/{idProduct}")
    public String deleteProduct(@PathVariable("idProduct") Long idProduct) {
        productRepository.deleteById(idProduct);
        return "redirect:/products-list";
    }

    @GetMapping("/products-restaurant/{idRestaurant}")
    public String getProductsByRestaurant(@PathVariable("idRestaurant") Long idRestaurant,  Model model){
        model.addAttribute("products", productRepository.findAllByRestaurantId(idRestaurant));
        return "products-restaurant";
    }

}
