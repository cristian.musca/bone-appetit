package com.thebest.controller;

import com.thebest.model.adminModel.Restaurant;
import com.thebest.model.adminModel.Schedule;
import com.thebest.repository.RestaurantRepository;
import com.thebest.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class ScheduleController {

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private RestaurantRepository restaurantRepository;


    @GetMapping("/displayAddFormSchedule/{idRestaurant}")
    public String displayAddFormSchedule(@PathVariable(value = "idRestaurant") Long idRestaurant, Model model) {
        Schedule schedule = new Schedule();
        final Optional<Restaurant> optional = restaurantRepository.findById(idRestaurant);
        if (optional.isPresent()) {
            final Restaurant restaurant = optional.get();
            System.out.println("Restaurant Id: " + restaurant.getIdRestaurant());
            model.addAttribute("restaurant", restaurant);
            schedule.setRestaurant(restaurant);
            model.addAttribute("schedule", schedule);


        } else {
            new IllegalArgumentException("Invalid restaurant Id" + idRestaurant);
        }

        return "add-schedule";
    }

    @PostMapping("/schedules/add")
    public String addSchedule(@Valid @ModelAttribute("schedule") Schedule schedule,Model model,
                              BindingResult result){
        if (result.hasErrors()) {
            return "add-schedule";
        }
        try{

            scheduleRepository.save(schedule);


        }catch(Exception e){
            e.printStackTrace();
        } finally {
            final Optional<Restaurant> optional = restaurantRepository.findById(schedule.getRestaurant().getIdRestaurant());
            Schedule newSchedule = new Schedule();
            if (optional.isPresent()){
                final Restaurant restaurant = optional.get();
                System.out.println("Id restaurant: "+ restaurant.getIdRestaurant());

                newSchedule.setRestaurant(restaurant);
                model.addAttribute("restaurant", restaurant);
                model.addAttribute("schedule", newSchedule);

            }

        }
        return "add-schedule";
    }

    @GetMapping("/schedules-list")
    public String displayScheduleList(Model model) {
        model.addAttribute("schedules", scheduleRepository.findAll());
        return "schedules-list";
    }

    @GetMapping("/schedules-restaurant/{idRestaurant}")
    public String displayScheduleRestaurant(@RequestParam("idRestaurant") Long idRestaurant, Model model) {
        model.addAttribute("schedules", scheduleRepository.findAllByRestaurantId(idRestaurant));
        return "/schedules-restaurant";
    }

    @GetMapping("/displayEditSchedule/{idSchedule}")
    public String displayUpdateForm(@PathVariable("idSchedule") Long idSchedule, Model model){
        final Optional<Schedule> optionalSchedule = scheduleRepository.findById(idSchedule);
        if (optionalSchedule.isPresent()) {
            final Schedule schedule = optionalSchedule.get();
            model.addAttribute("schedule", schedule);
        } else {
            new IllegalArgumentException("Invalid schedule Id:" + idSchedule);
        }

        return "update-schedule";
    }

    @PostMapping("/schedules/update/{idSchedule}")
    public String updateProduct(@PathVariable("idSchedule") Long idSchedule,
                                @Valid @ModelAttribute("schedule") Schedule schedule, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "update-schedule";
        }
        scheduleRepository.save(schedule);
        return "redirect:/schedules-list";
    }

    @GetMapping("/schedules/delete/{idSchedule}")
    public String deleteSchedule(@PathVariable("idSchedule") Long idSchedule) {
        scheduleRepository.deleteById(idSchedule);
        return "redirect:/schedules-list";
    }

}
