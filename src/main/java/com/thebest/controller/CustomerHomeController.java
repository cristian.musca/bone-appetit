package com.thebest.controller;

import com.thebest.model.adminModel.Product;
import com.thebest.model.adminModel.Restaurant;
import com.thebest.model.clientModel.Customer;
import com.thebest.model.clientModel.OrderCustomer;
import com.thebest.model.clientModel.OrderItem;
import com.thebest.model.clientModel.Status;
import com.thebest.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Controller
public class CustomerHomeController {
    @Autowired
    RestaurantRepository restaurantRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    OrderCustomerRepository orderCustomerRepository;

    @Autowired
    OrderItemRepository orderItemRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    private HttpSession httpSession;

    @GetMapping("/displayCustomerHome")
    public String displayClientHome(Model model){
        model.addAttribute("restaurants", restaurantRepository.findAll());
        return "customer-home";
    }

    @GetMapping("/productsRestaurant/{idRestaurant}")
    public String getProductsByRestaurant(@PathVariable("idRestaurant") Long idRestaurant,
                                          Model model)
                                         {

        List<OrderItem>orderItemList = new ArrayList<>();
        OrderCustomer orderCustomer = new OrderCustomer();
         Customer customer = (Customer) httpSession.getAttribute("customer");
        for(Product product:productRepository.findAllByRestaurantId(idRestaurant)){
            OrderItem orderItem = new OrderItem(product, orderCustomer);
            orderItemList.add(orderItem);
            Restaurant restaurant = orderItem.getProduct().getRestaurant();
            orderCustomer.setRestaurant(restaurant);

        }

        orderCustomer.setOrderItemList(orderItemList);

        orderCustomer.setCustomer(customer);
        orderCustomer.setDate(LocalDate.now());
        orderCustomer.setStatus(Status.AFFECTED);
        orderCustomer.setTime(LocalTime.now());
        model.addAttribute("orderCustomer", orderCustomer);
        model.addAttribute("customer", customer);

        return "order-items";
    }

    //List<OrderItem> nonZeroOi = new ArrayList<>();
    //for(OrderItem orderItem :orderCustomer.getOrderItemList()) {
    //    if(orderItem.getQuantity() > 0) {
    //        nonZeroOi.add(orderItem);
    //    }
    //}
    //orderCustomer.setOrderItemList(nonZeroOi);

    @Transactional
    @PostMapping("orders/add")
    public String saveOrder(@Valid @ModelAttribute("orderCustomer")OrderCustomer orderCustomer,
                            Model model,BindingResult result){
        if (result.hasErrors()) {
            return "orders-item";
        }
        try{

        }catch(Exception e){
            e.printStackTrace();
        }


        orderCustomer.getOrderItemList().stream().forEach(orderItem -> orderItem.setOrderCustomer(orderCustomer));
        List<OrderItem> nonZeroOi = new ArrayList<>();
        for(OrderItem orderItem :orderCustomer.getOrderItemList()) {
            if(orderItem.getQuantity() > 0) {
                nonZeroOi.add(orderItem);
            }
        }
        orderCustomer.setOrderItemList(nonZeroOi);

        orderCustomerRepository.save(orderCustomer);
        model.addAttribute("orderCustomer", orderCustomer);

        Long id = orderCustomer.getCustomer().getIdCustomer();
        Customer customer = customerRepository.findById(id).get();
        model.addAttribute("customer",customer);

        return "cart";
    }


    @GetMapping("/cart")
    public String displayOrder(@ModelAttribute ("orderCustomer")OrderCustomer orderCustomer){


        return "cart";
    }

    private byte[] convertToBytes(MultipartFile file) throws IOException {
        byte[] byteObjects = new byte[file.getBytes().length];
        int i = 0;
        for (byte b : file.getBytes()) {
            byteObjects[i++] = b;
        }
        return byteObjects;
    }


}
