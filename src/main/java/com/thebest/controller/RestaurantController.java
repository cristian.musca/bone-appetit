package com.thebest.controller;

import com.thebest.model.adminModel.Manager;
import com.thebest.model.adminModel.Restaurant;
import com.thebest.repository.ManagerRepository;
import com.thebest.repository.ProductRepository;
import com.thebest.repository.RestaurantRepository;
import com.thebest.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Optional;

@Controller
public class RestaurantController {

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Autowired
    private ManagerRepository managerRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ScheduleRepository scheduleRepository;



    @GetMapping("/displayAddFormRestaurant/{idManager}")
    public String displayAddForm(@PathVariable(value = "idManager") Long idManager, Model model) {

        Restaurant restaurant = new Restaurant();

        final Optional<Manager> optional = managerRepository.findById(idManager);
        if (optional.isPresent()) {
            final Manager manager = optional.get();
            model.addAttribute("manager", manager);
            restaurant.setManager(manager);
            model.addAttribute("restaurant", restaurant);
        }
        else {
            new IllegalArgumentException("Invalid manager Id" + idManager);
        }

        return "add-restaurant";
    }



    public byte[] convertToBytes(MultipartFile file) throws IOException {
        byte[] byteObjects = new byte[file.getBytes().length];
        int i = 0;
        for (byte b : file.getBytes()) {
            byteObjects[i++] = b;
        }
        return byteObjects;
    }

    @PostMapping("/restaurants/add")
    public String addRestaurant(@Valid @ModelAttribute("restaurant") Restaurant restaurant,
                                BindingResult result,
                                @RequestParam("imageFile") MultipartFile file ) {
        if (result.hasErrors()) {
            return "add-restaurant";
        }
        try{
            byte[] byteObjects = convertToBytes(file);
            restaurant.setImage(byteObjects);
            restaurantRepository.save(restaurant);

        }catch(Exception e){
            e.printStackTrace();
        }

        for(Restaurant restaurant1: restaurantRepository.findAll()){
            restaurant1.displayRestaurant();
        }
        return "redirect:/managers-list";
    }

    @GetMapping("/restaurants-list")
    public String displayRestaurantsList(Model model) {
        model.addAttribute("restaurants", restaurantRepository.findAll());

        return "restaurants-list";
    }

    @GetMapping("/restaurants-manager/{idManager}")
    public String displayRestaurantsManager(@PathVariable("idManager") Long idManager,  Model model){
        model.addAttribute("restaurants", restaurantRepository.findAllByManagerId(idManager));
        return "restaurants-manager";
    }

    @GetMapping("/displayEditRestaurant/{idRestaurant}")
    public String displayUpdateRestaurant(@PathVariable("idRestaurant") Long idRestaurant, Model model){
        final Optional<Restaurant> optionalRestaurant = restaurantRepository.findById(idRestaurant);
        if (optionalRestaurant.isPresent()) {
            final Restaurant restaurant = optionalRestaurant.get();
            model.addAttribute("restaurant", restaurant);
        } else {
            new IllegalArgumentException("Invalid restaurant Id:" + idRestaurant);
        }

        return "update-restaurant";
    }

    @PostMapping("/restaurants/update/{idRestaurant}")
    public String updateRestaurant(@PathVariable("idRestaurant") Long idRestaurant,
                                   @Valid @ModelAttribute("restaurant")  Restaurant restaurant,
                                   @RequestParam("imageFile") MultipartFile file ,
                                   BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "update-restaurant";
        }
        try{
            byte[] byteObjects = convertToBytes(file);
            restaurant.setImage(byteObjects);
            restaurantRepository.save(restaurant);

        }catch(Exception e){
            e.printStackTrace();
        }

        return "redirect:/managers-list";
    }

    @GetMapping("/restaurants/delete/{idRestaurant}")
    public String deleteRestaurant(@PathVariable("idRestaurant") Long idRestaurant) {
        restaurantRepository.deleteById(idRestaurant);
        return "redirect:/managers-list";
    }

}
