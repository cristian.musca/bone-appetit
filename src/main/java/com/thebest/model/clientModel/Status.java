package com.thebest.model.clientModel;

public enum Status {
    AFFECTED,
    ACCEPTED,
    REFUSED,
    DELIVERED
}
